/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network;

import com.dzingishan.ktogameserver.game.ServerGameProcess;
import com.dzingishan.ktogameserver.network.packets.PacketManager;
import com.dzingishan.ktogameserver.network.packets.auth.outgoing.P10GameFinish;
import com.dzingishan.ktogameserver.network.packets.auth.outgoing.P11ServerReady;
import com.dzingishan.ktogameserver.servermanager.NettyServerManager;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.game.PlayerDetails;

/**
 * Main Server Class, starts server
 *
 * @author dzing
 */
public class GameServer {

    public NettyServerManager manager;

    /**
     * GameServer version
     */
    public int VERSION;

    public int gameNumber;

    public GameServer(int version, NettyServerManager manager) {
        this.VERSION = version;
        this.manager = manager;
    }

    private int port;
    private PacketManager packetManager;
    private ServerGameProcess gameProcess;

    /**
     * Gets port of current server
     *
     * @return
     */
    public int getPort() {
        return port;
    }

    private EventLoopGroup bossGroup, workerGroup;
    private ChannelFuture channel;

    /**
     * Starts new GameServer and listens for players on this channel
     *
     * @param players
     * @param PORT
     */
    public void start(PlayerDetails[] players, int PORT, int gameNumber) {
        System.out.println("Starting game");
        this.gameNumber = gameNumber;
        try {
            this.port = PORT;
            gameProcess = new ServerGameProcess(this, players); //Creates new gameProcess
            packetManager = new PacketManager(gameProcess);//Creates new PacketManager

            //Creates new EventLoopGroups: Boss and worker
            bossGroup = new NioEventLoopGroup();
            workerGroup = new NioEventLoopGroup();

            //New server
            ServerBootstrap b = new ServerBootstrap();
            //Groups Boss and Worker
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class) //Sets channel type
                    .childHandler(new ChannelInitializer<SocketChannel>() { //Creates ChannelInitializer and adds all handlers to it
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new PacketEncoder()).addLast(new PacketDecoder(gameProcess)).addLast(new InboundPacketHandler(packetManager));
                        }
                    })
                    //Options
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            channel = b.bind(PORT); //Bind server to given channel
            P11ServerReady p = new P11ServerReady();
            p.gameNumber = gameNumber;
            manager.f.channel().writeAndFlush(p);
            gameProcess.startGame();
            for (CharacterBase d : gameProcess.getCardDatabase().getCharactersBaseList()) {
                System.out.println(d.uniqueNumber + " " + d.UniqueName);
            }
        } catch (Exception ex) {
            Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Stops current GameServer
     */
    public void stop() {
        channel.channel().closeFuture();
        workerGroup.shutdownGracefully();
        bossGroup.shutdownGracefully();
        gameProcess.stop();
        P10GameFinish p = new P10GameFinish();
        p.serverNumber = manager.serverNumber;
        p.gameNumber = gameNumber;
        manager.f.channel().writeAndFlush(p);
    }

}
