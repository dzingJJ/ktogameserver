/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network;

import com.dzingishan.ktogameserver.network.packets.Packet;
import io.netty.channel.Channel;
import java.net.SocketAddress;

/**
 * Connection instance, used to identify the player
 *
 * @author dzing
 */
public class ConnectionInstance {

    private final Channel connChannel;
    private final SocketAddress address;

    /**
     * Gets current remoteAddress
     *
     * @return Remote Address
     */
    public SocketAddress getAddress() {
        return address;
    }

    /**
     * Gets current Channel through which player connects
     *
     * @return Channel
     */
    public Channel getChannel() {
        return connChannel;
    }

    public ConnectionInstance(Channel connChannel, SocketAddress address) {
        this.connChannel = connChannel;
        this.address = address;
    }

    /**
     * Send packet through channel
     *
     * @param packet Packet to send
     */
    public void send(Packet packet) {
        connChannel.writeAndFlush(packet);
    }

}
