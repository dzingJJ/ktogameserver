/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.auth.ingoing;

import com.dzingishan.ktogameserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P28ServerNumber extends ServerBoundPacket {

    public P28ServerNumber() {
        super(28);
    }

    public int serverNumber;

    @Override
    public void read(ByteBuf buff) {
        serverNumber = buff.readInt();
    }

}
