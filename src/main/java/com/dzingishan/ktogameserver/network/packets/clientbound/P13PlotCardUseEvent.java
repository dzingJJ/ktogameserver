/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P13PlotCardUseEvent extends ClientBoundPacket {

    public P13PlotCardUseEvent() {
        super(new PacketFrame(13));
    }

    public int executor, receiver, card;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(executor).writeInt(receiver).writeInt(card);
    }

    @Override
    public int getLength() {
        return 12;
    }

}
