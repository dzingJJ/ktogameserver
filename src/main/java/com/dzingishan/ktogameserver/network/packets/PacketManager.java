/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets;

import com.dzingishan.ktogameserver.game.ServerGameProcess;
import com.dzingishan.ktogameserver.network.login.LoginPacketManager;
import com.dzingishan.ktogameserver.network.packets.login.P18LoginData;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P01ClientKeepalivePacket;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P02GameDataRequest;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P03PlayersDataRequest;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P04OwnDataRequest;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P05PassiveUse;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P06PlotCardUse;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P07ClaimDeath;
import io.netty.channel.Channel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Packet Manger, used for getting correct packets and bridges between Game and
 * Server
 *
 * @author dzing
 */
public class PacketManager {

    private final static Map<Integer, Class<? extends ServerBoundPacket>> SERVER_BOUND = new HashMap<>();
    // private final static Map<Integer, Class<? extends ClientBoundPacket>> CLIENT_BOUND = new HashMap<>();
    private final static Map<Integer, Class<? extends Packet>> LOGIN_BOUND = new HashMap<>();

    static {

        SERVER_BOUND.put(18, P18LoginData.class);

        SERVER_BOUND.put(1, P01ClientKeepalivePacket.class);
        SERVER_BOUND.put(2, P02GameDataRequest.class);
        SERVER_BOUND.put(3, P03PlayersDataRequest.class);
        SERVER_BOUND.put(4, P04OwnDataRequest.class);
        SERVER_BOUND.put(5, P05PassiveUse.class);
        SERVER_BOUND.put(6, P06PlotCardUse.class);
        SERVER_BOUND.put(7, P07ClaimDeath.class);
        //CLIENT_BOUND.put(1, P01ServerKeepalivePacket.class);
        //serverBound.put(1, P01StartLogin.class);
    }

//    /**
//     * Gets Empty LoginBound packets
//     *
//     * @param frame Frame according to which packet will be get
//     * @return Empty LoginBound packet with frame, otherwise null
//     */
//    public static Packet getLoginBoundPacket(PacketFrame frame) {
//        Class<? extends Packet> c = LOGIN_BOUND.get(frame.number);
//        if (c != null) {
//            try {
//                Packet p = c.newInstance();
//                p.frame = frame;
//                return p;
//            } catch (InstantiationException | IllegalAccessException ex) {
//                Logger.getLogger(PacketManager.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        return null;
//    }
    /**
     * Gets empty ServeBound packet
     *
     * @param frame Frame to get packet based on
     * @return
     */
    public static Packet getServerBoundPacket(PacketFrame frame) {
        Class<? extends Packet> c = SERVER_BOUND.get(frame.number);
        if (c != null) {
            try {
                Packet p = c.newInstance();
                p.frame = frame;
                return p;
            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(PacketManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    private final ServerGameProcess gameProcess;

    private final LoginPacketManager loginPacketManager;
    private final ServerPacketManager serverPacketManager;

    public PacketManager(ServerGameProcess gameProcess) {
        this.gameProcess = gameProcess;
        loginPacketManager = new LoginPacketManager(gameProcess);
        serverPacketManager = new ServerPacketManager(gameProcess);
    }

    /**
     * Processing incomming Packet, bridges it between Login and Server manager
     *
     * @param packet Packet to process
     * @param channel Channel to get
     */
    public void processIncomingPacket(Packet packet, Channel channel) {
        if (!isLoggedIn(channel)) {
            loginPacketManager.incomingPacket(packet, channel);
        } else {
            serverPacketManager.incomingPacket(packet, gameProcess.getPlayer(channel));
        }
    }

    public boolean isLoggedIn(Channel channel) {
        return gameProcess.hasLoggedIn(channel);
    }

    public void connection(Channel channel) {
        //gameProcess.playerConnection(channel);
    }

    public void disconnect(Channel channel) {
        gameProcess.playerDisconnected(channel);
    }
}
