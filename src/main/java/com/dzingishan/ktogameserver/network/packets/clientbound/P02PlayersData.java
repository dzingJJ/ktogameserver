/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;
import java.nio.charset.Charset;
import java.util.List;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Contains SerializedPlayerData
 *
 * @author dzing
 */
public class P02PlayersData extends ClientBoundPacket {

    public SerializedPlayerData[] players;

    /**
     * Player who has execution card
     */
    public int execution;

    public P02PlayersData() {
        super(new PacketFrame(2));
    }

    /**
     * Gets packet from current process
     *
     * @param process Game process to get data from
     * @return P02PlayersData packet
     */
    public static P02PlayersData getPacket(GameProcess process) {
        P02PlayersData packet = new P02PlayersData();
        List<Player> players = process.getGame().Players;
        packet.players = new SerializedPlayerData[players.size()];
        for (int i = 0; i < players.size(); i++) {
            Player player = players.get(i);
            packet.players[i] = new SerializedPlayerData(player.PlayerDetails.Nickname, player.PlayerDetails.UniqueNumber, player.Died, player.CurrentCharacter.uniqueNumber, player.Coins, player.PlotCards.size());
        }
        packet.execution = process.getGame().PlayerWithExecution.PlayerDetails.UniqueNumber;
        return packet;
    }

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(execution);
        buff.writeInt(players.length);
        for (SerializedPlayerData player : players) {
            byte[] username = player.username.getBytes(Charset.forName("UTF8"));
            buff.writeInt(username.length).writeBytes(username).writeInt(player.uuid).writeInt(player.characterCard).writeInt((player.coins)).writeInt(player.numberOfPlotCards).writeBoolean(player.died);
        }

    }

    @Override
    public int getLength() {
        return 0;
    }

    /**
     * Contains serialized data of the player
     */
    public static class SerializedPlayerData {

        public String username;
        public int uuid;

        public boolean died;
        public int characterCard;
        public int coins, numberOfPlotCards;

        public SerializedPlayerData() {
        }

        public SerializedPlayerData(String username, int uuid, boolean died, int characterCard, int coins, int numberOfPlotCards) {
            this.username = username;
            this.uuid = uuid;
            this.died = died;
            this.characterCard = characterCard;
            this.coins = coins;
            this.numberOfPlotCards = numberOfPlotCards;
        }

    }

}
