/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P16PlotCardDeleteEvent extends ClientBoundPacket {

    public P16PlotCardDeleteEvent() {
        super(new PacketFrame(16));
    }

    public int player;
    public int card;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(player).writeInt(card);
    }

    @Override
    public int getLength() {
        return 8;
    }

}
