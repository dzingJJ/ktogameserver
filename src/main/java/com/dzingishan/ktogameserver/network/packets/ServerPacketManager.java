/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets;

import com.dzingishan.ktogameserver.game.ConnectedPlayer;
import com.dzingishan.ktogameserver.game.ServerGameProcess;
import com.dzingishan.ktogameserver.network.packets.clientbound.P01ServerKeepalivePacket;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P01ClientKeepalivePacket;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P02GameDataRequest;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P03PlayersDataRequest;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P04OwnDataRequest;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P05PassiveUse;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P06PlotCardUse;
import com.dzingishan.ktogameserver.network.packets.serverbounds.P07ClaimDeath;

/**
 * Manager of ServerBound packets
 *
 * @author dzing
 */
public class ServerPacketManager {

    private ServerGameProcess gameProcess;

    public ServerPacketManager(ServerGameProcess gameProcess) {
        this.gameProcess = gameProcess;
    }

    /**
     * Proceess incoming ServerBound packet assosiated with ConnectedPlayer
     *
     * @param p Packet to process
     * @param player Player assisiated with Packet
     */
    public void incomingPacket(Packet p, ConnectedPlayer player) {
        if (!(p instanceof ServerBoundPacket)) {
            System.out.println("ERRORR!R!!");//TODO
        }

        ServerBoundPacket packet = (ServerBoundPacket) p;

        if (packet instanceof P01ClientKeepalivePacket) {
            player.sendPacket(new P01ServerKeepalivePacket());
        } else if (packet instanceof P02GameDataRequest) {
            gameProcess.sendGameData(player);
        } else if (packet instanceof P03PlayersDataRequest) {
            gameProcess.sendPlayersData(player);
        } else if (packet instanceof P04OwnDataRequest) {
            gameProcess.sendOwnData(player);
        } else if (packet instanceof P05PassiveUse) {
            gameProcess.usePassive(player, gameProcess.getPlayerByUUID(((P05PassiveUse) packet).target));
        } else if (packet instanceof P06PlotCardUse) {
            P06PlotCardUse pa = (P06PlotCardUse) packet;
            gameProcess.usePlotCard(player, gameProcess.getPlayerByUUID(pa.target), gameProcess.getCardDatabase().getCard(pa.card));
        } else if (packet instanceof P07ClaimDeath) {
            gameProcess.claimDeath(player);
        }
    }

}
