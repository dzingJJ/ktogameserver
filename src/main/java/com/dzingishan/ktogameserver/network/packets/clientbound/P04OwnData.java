/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;
import java.nio.charset.Charset;
import org.dzingishan.killtheoverlord.game.Player;

/**
 *
 * @author dzing
 */
public class P04OwnData extends ClientBoundPacket {

    public int currentCharacter, coins;
    public int[] plotcards;
    public boolean died;
    public int uuid;
    public String username;

    public P04OwnData() {
        super(new PacketFrame(4));
    }

    public static P04OwnData getOwnData(Player player) {
        P04OwnData own = new P04OwnData();
        own.currentCharacter = player.CurrentCharacter.uniqueNumber;
        own.coins = player.Coins;
        own.plotcards = new int[player.PlotCards.size()];
        for (int i = 0; i < own.plotcards.length; i++) {
            own.plotcards[i] = player.PlotCards.get(i).getUniqueId();
        }
        own.died = player.Died;
        own.username = player.PlayerDetails.Nickname;
        own.uuid = player.PlayerDetails.UniqueNumber;
        return own;
    }

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(currentCharacter).writeInt(coins).writeBoolean(died).writeInt(plotcards.length);
        for (int i = 0; i < plotcards.length; i++) {
            buff.writeInt(plotcards[i]);
        }
        byte[] str = username.getBytes(Charset.forName("UTF8"));
        buff.writeInt(uuid).writeInt(str.length).writeBytes(str);
    }

    @Override
    public int getLength() {
        return 0;
    }

}
