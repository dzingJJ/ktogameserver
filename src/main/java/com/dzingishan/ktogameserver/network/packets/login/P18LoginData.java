/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.login;

import com.dzingishan.ktogameserver.network.login.LoginData;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import com.dzingishan.ktogameserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;
import java.nio.charset.Charset;

/**
 * Packet sended by client to server with LoginData needed for authorization
 *
 * @author dzing
 */
public class P18LoginData extends ServerBoundPacket {

    public LoginData data;

    public P18LoginData() {
        super(new PacketFrame(18));
    }

    @Override
    public void read(ByteBuf buff) {
        int uuid = buff.readInt();
        int token = buff.readInt();
        int l = buff.readInt();
        byte[] b = new byte[l];
        buff.readBytes(b);
        String username = new String(b, Charset.forName("UTF8"));
        l = buff.readInt();
        b = new byte[l];
        buff.readBytes(b);
        String email = new String(b, Charset.forName("UTF8"));
        data = new LoginData(email, token, username, uuid);
    }

}
