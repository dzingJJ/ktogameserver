/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets;

import io.netty.buffer.ByteBuf;
import java.nio.charset.Charset;

/**
 * Base class of any Packet
 *
 * @author dzing
 */
public abstract class Packet {

    public static String readString(ByteBuf buff) {
        int len = buff.readInt();
        byte[] buffer = new byte[len];
        buff.readBytes(buffer);
        return new String(buffer, Charset.forName("UTF8"));
    }

    public static void writeString(ByteBuf buff, String s) {
        byte[] buffer = s.getBytes(Charset.forName("UTF8"));
        buff.writeInt(buffer.length).writeBytes(buffer);
    }

    /**
     * PacketFrame contains fixed-length data of Packet
     */
    public PacketFrame frame;

    public Packet(PacketFrame frame) {
        this.frame = frame;
    }

    /**
     * Called when receiving packet and reading it from bytes
     *
     * @param buff Buffer to read from
     */
    public abstract void read(ByteBuf buff);

    /**
     * Called when sending packet and writing it to bytes
     *
     * @param buff
     */
    public abstract void write(ByteBuf buff);

    /**
     * Gets length of packet, need in same cases for Frame
     *
     * @return
     */
    public abstract int getLength();
}
