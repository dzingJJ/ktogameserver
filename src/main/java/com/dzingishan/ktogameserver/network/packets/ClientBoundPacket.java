/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets;

import io.netty.buffer.ByteBuf;

/**
 * Base class for client-bound packets (sended from server to client)
 *
 * @author dzing
 */
public abstract class ClientBoundPacket extends Packet {

    public ClientBoundPacket(PacketFrame frame) {
        super(frame);
    }

    public ClientBoundPacket(int nr) {
        super(new PacketFrame(nr));
    }

    /**
     * Read parameter is not used by server, so you don't really need
     * implementation here
     *
     * @param buff Buffer
     */
    @Override
    public void read(ByteBuf buff) {
    }

    @Override
    public int getLength() {
        return 0;
    }

}
