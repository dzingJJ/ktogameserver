/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P08DeathEvent extends ClientBoundPacket {

    public P08DeathEvent() {
        super(new PacketFrame(8));
    }
    public int killer, killed;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(killer).writeInt(killed);
    }

    @Override
    public int getLength() {
        return 8;
    }

}
