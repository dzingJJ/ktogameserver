/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P05ActionEvent extends ClientBoundPacket {

    public static enum Action {

        UNKNOWN(0), GAME_STARTED(1), PRE_GAME_START(2), ROUND_END(3), EXECUTION_END(4);

        private int number;

        private Action(int number) {
            this.number = number;
        }

    }

    private Action action;

    public P05ActionEvent() {
        super(new PacketFrame(5));
        this.action = Action.UNKNOWN;
    }

    public P05ActionEvent(Action action) {
        super(new PacketFrame(5));
        this.action = action;
    }

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(action.number);
    }

    @Override
    public int getLength() {
        return 4;
    }

}
