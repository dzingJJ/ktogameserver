/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets;

import io.netty.buffer.ByteBuf;

/**
 * Base class for ServerBoundPackets (sended by client to server)
 *
 * @author dzing
 */
public abstract class ServerBoundPacket extends Packet {

    public ServerBoundPacket(PacketFrame frame) {
        super(frame);
    }

    public ServerBoundPacket(int nr) {
        super(new PacketFrame(nr));
    }

    /**
     * Server will only read packets, so write implementation is not needed
     *
     * @param buff
     */
    @Override
    public void write(ByteBuf buff) {
    }

    /**
     * Length is used during write process, so don't really need this
     * implementation
     *
     * @return
     */
    @Override
    public int getLength() {
        return 0;
    }
}
