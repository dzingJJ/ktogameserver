/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.auth.ingoing;

import com.dzingishan.ktogameserver.network.packets.Packet;
import com.dzingishan.ktogameserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;
import org.dzingishan.killtheoverlord.game.PlayerDetails;

/**
 *
 * @author dzing
 */
public class P29StartTheGame extends ServerBoundPacket {

    public P29StartTheGame() {
        super(29);
    }

    public int port, gameNumber;
    public PlayerDetails[] players;

    @Override
    public void read(ByteBuf buff) {
        port = buff.readInt();
        gameNumber = buff.readInt();
        int l = buff.readInt();
        players = new PlayerDetails[l];
        for (int i = 0; i < l; i++) {
            players[i] = new PlayerDetails(Packet.readString(buff), buff.readInt());
        }
    }

}
