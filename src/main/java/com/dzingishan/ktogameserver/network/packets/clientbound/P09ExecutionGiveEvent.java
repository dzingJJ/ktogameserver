/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P09ExecutionGiveEvent extends ClientBoundPacket {

    public P09ExecutionGiveEvent() {
        super(new PacketFrame(9));
    }

    public int from, to;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(from).writeInt(to);
    }

    @Override
    public int getLength() {
        return 8;
    }

}
