/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.auth.outgoing;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.Packet;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P06ServerRegister extends ClientBoundPacket {

    public P06ServerRegister() {
        super(6);
    }

    public String address;

    public int[] ports;

    @Override
    public void write(ByteBuf buff) {
        Packet.writeString(buff, address);
        buff.writeInt(ports.length);
        for (int port : ports) {
            buff.writeInt(port);
        }
    }

}
