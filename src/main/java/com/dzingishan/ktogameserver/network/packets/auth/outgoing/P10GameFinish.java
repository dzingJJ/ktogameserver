/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.auth.outgoing;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P10GameFinish extends ClientBoundPacket {

    public P10GameFinish() {
        super(10);
    }

    public int gameNumber, serverNumber;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(gameNumber).writeInt(serverNumber);
    }

}
