/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 * Keepalive packet, sended as ping of P01ClientKeepalivePacket
 *
 * @author dzing
 */
public class P01ServerKeepalivePacket extends ClientBoundPacket {

    public P01ServerKeepalivePacket() {
        super(new PacketFrame(1));
    }

    @Override
    public void write(ByteBuf buff) {
    }

    @Override
    public int getLength() {
        return 0;
    }

}
