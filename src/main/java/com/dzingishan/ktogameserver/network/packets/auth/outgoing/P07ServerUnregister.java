/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.auth.outgoing;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P07ServerUnregister extends ClientBoundPacket {

    public P07ServerUnregister() {
        super(7);
    }

    public int serverNumber;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(serverNumber);
    }

}
