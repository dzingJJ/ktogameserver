/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;
import org.dzingishan.killtheoverlord.events.CashModifyEvent;

/**
 *
 * @author dzing
 */
public class P06CashModifyEvent extends ClientBoundPacket {

    public P06CashModifyEvent() {
        super(new PacketFrame(6));
    }

    public int finalCoins;
    public int player;

    public static P06CashModifyEvent getPacket(CashModifyEvent event) {
        P06CashModifyEvent evt = new P06CashModifyEvent();
        evt.finalCoins = event.FinalCoins;
        evt.player = event.Player.PlayerDetails.UniqueNumber;
        return evt;
    }

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(player).writeInt(finalCoins);
    }

    @Override
    public int getLength() {
        return 8;
    }

}
