/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P15CharacterChangeEvent extends ClientBoundPacket {

    public P15CharacterChangeEvent() {
        super(new PacketFrame(15));
    }

    public int player, from, to;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(player).writeInt(from).writeInt(to);
    }

    @Override
    public int getLength() {
        return 12;
    }

}
