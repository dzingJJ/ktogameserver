/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.auth.outgoing;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P11ServerReady extends ClientBoundPacket {

    public P11ServerReady() {
        super(11);
    }

    public int gameNumber;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(gameNumber);
    }

}
