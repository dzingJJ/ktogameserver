/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P11PlotCardAddEvent extends ClientBoundPacket {

    public P11PlotCardAddEvent() {
        super(new PacketFrame(11));
    }

    public int player;

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(player);
    }

    @Override
    public int getLength() {
        return 4;
    }

}
