/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P03GameData extends ClientBoundPacket {

    public int characterCardsVersion, plotCardsVersion;

    public boolean hasStarted;

    public P03GameData() {
        super(new PacketFrame(3));
    }

    public P03GameData(int characterCardsVersion, int plotCardsVersion, boolean hasStarted) {
        super(new PacketFrame(3));
        this.characterCardsVersion = characterCardsVersion;
        this.plotCardsVersion = plotCardsVersion;
        this.hasStarted = hasStarted;
    }

    @Override
    public void write(ByteBuf buff) {
        buff.writeInt(characterCardsVersion).writeInt(plotCardsVersion).writeBoolean(hasStarted);
    }

    @Override
    public int getLength() {
        return 8;
    }

}
