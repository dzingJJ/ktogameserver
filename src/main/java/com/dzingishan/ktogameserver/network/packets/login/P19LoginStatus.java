/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.login;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 * Packet sended to client after P03LoginData, indicates if login was
 * successfull or not
 *
 * @author dzing
 */
public class P19LoginStatus extends ClientBoundPacket {

    public boolean loginSucessfull;

    public P19LoginStatus() {
        super(new PacketFrame(19));
    }

    @Override
    public void write(ByteBuf buff) {
        buff.writeBoolean(loginSucessfull);
    }

}
