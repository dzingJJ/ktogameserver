/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P17ExecuteError extends ClientBoundPacket {

    public P17ExecuteError() {
        super(17);
    }

    @Override
    public void write(ByteBuf buff) {

    }

}
