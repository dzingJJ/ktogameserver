/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.clientbound;

import com.dzingishan.ktogameserver.network.packets.ClientBoundPacket;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P07CharacterReshuffleEvent extends ClientBoundPacket {

    public P07CharacterReshuffleEvent() {
        super(new PacketFrame(7));
    }

    public boolean isRandom;

    @Override
    public void write(ByteBuf buff) {
        buff.writeBoolean(isRandom);
    }

    @Override
    public int getLength() {
        return 0;
    }

}
