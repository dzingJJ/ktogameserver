/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.auth.ingoing;

import com.dzingishan.ktogameserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P30Confirm extends ServerBoundPacket {

    public P30Confirm() {
        super(30);
    }

    @Override
    public void read(ByteBuf buff) {
    }

}
