/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.serverbounds;

import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import com.dzingishan.ktogameserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 * Sended by client to Server after 300ms on no-packet activity, to remain that
 * client is alive
 *
 * @author dzing
 */
public class P01ClientKeepalivePacket extends ServerBoundPacket {

    public P01ClientKeepalivePacket() {
        super(new PacketFrame(1));
    }

    @Override
    public void read(ByteBuf buff) {
    }

    @Override
    public int getLength() {
        return 0;
    }

}
