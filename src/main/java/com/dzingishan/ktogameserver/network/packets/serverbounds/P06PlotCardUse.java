/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.packets.serverbounds;

import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import com.dzingishan.ktogameserver.network.packets.ServerBoundPacket;
import io.netty.buffer.ByteBuf;

/**
 *
 * @author dzing
 */
public class P06PlotCardUse extends ServerBoundPacket {

    public int target, card;

    public P06PlotCardUse() {
        super(new PacketFrame(6));
    }

    @Override
    public void read(ByteBuf buff) {
        target = buff.readInt();
        card = buff.readInt();
    }

}
