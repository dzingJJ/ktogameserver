/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network.login;

import com.dzingishan.ktogameserver.game.ServerGameProcess;
import com.dzingishan.ktogameserver.network.packets.Packet;
import com.dzingishan.ktogameserver.network.packets.login.P18LoginData;
import com.dzingishan.ktogameserver.network.packets.login.P19LoginStatus;
import io.netty.channel.Channel;

/**
 * Manages login packets
 *
 * @author dzing
 */
public class LoginPacketManager {

    private ServerGameProcess gameProcess;

    public LoginPacketManager(ServerGameProcess process) {
        this.gameProcess = process;
    }

    /**
     * Called when player sends any login packet
     *
     * @param packet
     * @param channel
     */
    public void incomingPacket(Packet packet, Channel channel) {

        if (packet instanceof P18LoginData) {
            P18LoginData login = (P18LoginData) packet;
            if (login(channel, login.data) && gameProcess.playerLoggedIn(login.data, channel)) {
                System.out.println("LOGGED IN! PLAYER DETECTED!");
                P19LoginStatus status = new P19LoginStatus();
                status.loginSucessfull = true;
                channel.writeAndFlush(status);
                changeMode(channel);
            } else {
                channel.writeAndFlush(new P19LoginStatus());
                channel.close();
            }
        }
    }

    /**
     * Checks if player sended correct login data
     *
     * @param channel Channel of the player
     * @param data LoginData of the player
     * @return True if correct, otherwise false
     */
    private boolean login(Channel channel, LoginData data) {
        //TODO: Perform checking
        return true;
    }

    /**
     * Changes player mode to Online
     *
     * @param channel Channel on which mode must be changed
     */
    private void changeMode(Channel channel) {
        gameProcess.changeMode(channel);
    }

}
