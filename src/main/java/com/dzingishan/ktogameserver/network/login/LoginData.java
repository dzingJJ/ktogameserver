/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.      
 */
package com.dzingishan.ktogameserver.network.login;

/**
 * Used when exchanging data with client
 *
 * @author dzing
 */
public class LoginData {

    public final String email, username;
    public final int uuid, token;

    public LoginData(String email, int token, String username, int uuid) {
        this.email = email;
        this.token = token;
        this.username = username;
        this.uuid = uuid;
    }

}
