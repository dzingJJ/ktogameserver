/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network;

import com.dzingishan.ktogameserver.game.ServerGameProcess;
import com.dzingishan.ktogameserver.network.packets.Packet;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import com.dzingishan.ktogameserver.network.packets.PacketManager;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.util.List;

/**
 * Class which decodes bytes to packet
 *
 * @author dzing
 */
public class PacketDecoder extends ByteToMessageDecoder {

    private final ServerGameProcess procces;

    public PacketDecoder(ServerGameProcess process) {
        this.procces = process;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        //System.out.println("Reading packet");
        if (in.readableBytes() < 4) {
            return;
        }
        int index = in.readerIndex();
        int l = in.readInt();
        if (in.readableBytes() < l) {
            in.readerIndex(index);
            return;
        }
        byte[] bytes = new byte[l];
        in.readBytes(bytes);
        //System.out.println(Arrays.toString(bytes));
        ByteBuf b = Unpooled.copiedBuffer(bytes);
        PacketFrame frame = new PacketFrame(b.readInt());
        Packet p = PacketManager.getServerBoundPacket(frame);
        p.read(b);
        out.add(p);

    }

}
