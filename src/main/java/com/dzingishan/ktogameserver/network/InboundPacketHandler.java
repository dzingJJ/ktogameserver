/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.network;

import com.dzingishan.ktogameserver.network.packets.Packet;
import com.dzingishan.ktogameserver.network.packets.PacketManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * Handler of Inbound Packets
 *
 * @author dzing
 */
public class InboundPacketHandler extends ChannelInboundHandlerAdapter {

    /**
     * Packet manager used for managing packets
     */
    private final PacketManager packetManager;

    public InboundPacketHandler(PacketManager packetManager) {
        this.packetManager = packetManager;
    }

    @Override
    /**
     * Called when system receives processed Packet
     */
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("Incoming packet: " + ((Packet) msg).getClass().getSimpleName()); //TODO: Remove
        packetManager.processIncomingPacket((Packet) msg, ctx.channel());
    }

    @Override
    /**
     * Called when exception is called TODO: How we should handle exceptions?
     */
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause.getClass().getSimpleName() + " " + cause.getMessage() + " ERRROR");
        cause.printStackTrace();//TODO: Remove and rewrite
    }

    @Override
    /**
     * Called when player logged in
     */
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        packetManager.connection(ctx.channel());
        super.channelRegistered(ctx); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    /**
     * Called when player disconnects
     */
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("Disconnect");
        packetManager.disconnect(ctx.channel());
        super.channelUnregistered(ctx); //To change body of generated methods, choose Tools | Templates.
    }

}
