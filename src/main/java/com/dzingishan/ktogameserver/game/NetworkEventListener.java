/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.game;

import com.dzingishan.ktogameserver.network.packets.clientbound.P05ActionEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P06CashModifyEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P07CharacterReshuffleEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P08DeathEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P09ExecutionGiveEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P10PassiveUseEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P11PlotCardAddEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P12OwnPlotCardAddEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P13PlotCardUseEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P14WinEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P15CharacterChangeEvent;
import com.dzingishan.ktogameserver.network.packets.clientbound.P16PlotCardDeleteEvent;
import org.dzingishan.killtheoverlord.events.CashModifyEvent;
import org.dzingishan.killtheoverlord.events.CharacterChangeEvent;
import org.dzingishan.killtheoverlord.events.CharacterReshuffleEvent;
import org.dzingishan.killtheoverlord.events.DeathEvent;
import org.dzingishan.killtheoverlord.events.Event;
import org.dzingishan.killtheoverlord.events.ExecutionEndEvent;
import org.dzingishan.killtheoverlord.events.ExecutionGiveEvent;
import org.dzingishan.killtheoverlord.events.GameStartEvent;
import org.dzingishan.killtheoverlord.events.PassiveUseEvent;
import org.dzingishan.killtheoverlord.events.PlotCardAddEvent;
import org.dzingishan.killtheoverlord.events.PlotCardRemoveEvent;
import org.dzingishan.killtheoverlord.events.PlotCardUseEvent;
import org.dzingishan.killtheoverlord.events.PreGameStartEvent;
import org.dzingishan.killtheoverlord.events.RoundEndEvent;
import org.dzingishan.killtheoverlord.events.WinEvent;
import org.dzingishan.killtheoverlord.game.EventListener;

/**
 * Main listener for all events from game
 *
 * @author dzing
 */
public class NetworkEventListener extends EventListener<Event> {

    private ServerGameProcess process;

    public NetworkEventListener(ServerGameProcess process) {
        this.process = process;
    }

    @Override
    public void run(Event event) {
        System.out.println(event.getClass().getSimpleName());
        if (event instanceof GameStartEvent) {
            process.sendPacketToAllPlayers(new P05ActionEvent(P05ActionEvent.Action.GAME_STARTED));
        } else if (event instanceof PreGameStartEvent) {
            process.sendPacketToAllPlayers(new P05ActionEvent(P05ActionEvent.Action.PRE_GAME_START));
        } else if (event instanceof CashModifyEvent) {
            CashModifyEvent e = (CashModifyEvent) event;
            P06CashModifyEvent p = new P06CashModifyEvent();
            p.finalCoins = e.FinalCoins;
            p.player = e.Player.getUniqueId();
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof CharacterChangeEvent) {
            CharacterChangeEvent e = (CharacterChangeEvent) event;
            P15CharacterChangeEvent p = new P15CharacterChangeEvent();
            p.player = e.player.getUniqueId();
            if (e.oldChara != null) {
                p.from = e.oldChara.uniqueNumber;
            } else {
                p.from = 0;
            }
            p.to = e.newChara.uniqueNumber;
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof CharacterReshuffleEvent) {
            CharacterReshuffleEvent evt = (CharacterReshuffleEvent) event;
            P07CharacterReshuffleEvent p = new P07CharacterReshuffleEvent();
            p.isRandom = evt.RandomReshuffle;
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof DeathEvent) {
            DeathEvent evt = (DeathEvent) event;
            P08DeathEvent p = new P08DeathEvent();
            p.killed = evt.Killed.getUniqueId();
            if (evt.Killer != null) {
                p.killer = evt.Killer.getUniqueId();
            } else {
                p.killer = 0;
            }
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof ExecutionGiveEvent) {
            ExecutionGiveEvent evt = (ExecutionGiveEvent) event;
            P09ExecutionGiveEvent p = new P09ExecutionGiveEvent();
            p.from = evt.From.getUniqueId();
            p.to = evt.To.getUniqueId();
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof PassiveUseEvent) {
            PassiveUseEvent evt = (PassiveUseEvent) event;
            P10PassiveUseEvent p = new P10PassiveUseEvent();
            p.card = evt.Executor.CurrentCharacter.uniqueNumber;
            p.executor = evt.Executor.getUniqueId();
            p.receiver = evt.Receiver.getUniqueId();
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof PlotCardAddEvent) {
            PlotCardAddEvent evt = (PlotCardAddEvent) event;
            P11PlotCardAddEvent p = new P11PlotCardAddEvent();
            p.player = evt.PlayerAdded.getUniqueId();
            process.sendPacketToAllPlayers(p);
            P12OwnPlotCardAddEvent pl = new P12OwnPlotCardAddEvent();
            pl.card = evt.Card.getUniqueId();
            ((ConnectedPlayer) evt.PlayerAdded).sendPacket(pl);
        } else if (event instanceof PlotCardUseEvent) {
            PlotCardUseEvent evt = (PlotCardUseEvent) event;
            P13PlotCardUseEvent p = new P13PlotCardUseEvent();
            p.card = evt.Card.getUniqueId();
            p.executor = evt.Executor.getUniqueId();
            p.receiver = evt.Receiver.getUniqueId();
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof PlotCardRemoveEvent) {
            PlotCardRemoveEvent evt = (PlotCardRemoveEvent) event;
            P16PlotCardDeleteEvent p = new P16PlotCardDeleteEvent();
            p.card = evt.card.getUniqueId();
            p.player = evt.player.getUniqueId();
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof WinEvent) {
            WinEvent evt = (WinEvent) event;
            P14WinEvent p = new P14WinEvent();
            p.player = evt.Winner.getUniqueId();
            process.sendPacketToAllPlayers(p);
        } else if (event instanceof RoundEndEvent) {
            process.sendPacketToAllPlayers(new P05ActionEvent(P05ActionEvent.Action.ROUND_END));
        } else if (event instanceof ExecutionEndEvent) {
            process.sendPacketToAllPlayers(new P05ActionEvent(P05ActionEvent.Action.EXECUTION_END));
        }
    }

}
