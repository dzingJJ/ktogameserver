/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.game;

import com.dzingishan.ktogameserver.network.ConnectionInstance;
import com.dzingishan.ktogameserver.network.packets.Packet;
import com.dzingishan.ktogameserver.network.packets.ServerBoundPacket;
import com.dzingishan.ktogameserver.network.packets.clientbound.P12OwnPlotCardAddEvent;
import io.netty.channel.Channel;
import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Override of {@link Player} class, used with online-gaming
 *
 * @author dzing
 */
public class ConnectedPlayer extends Player {

    private boolean loaded = false;

    boolean loaded() {
        return loaded;
    }

    /**
     * List of availible connection statuses
     */
    public static enum ConnectionStatus {
        NO_CONNECTED_ONCE, DISCONNECTED, ONLINE, LEFT, LOGIN
    }

    /**
     * Connection instance, storing current player connection info
     */
    private ConnectionInstance connection;

    /**
     * Gets connection
     *
     * @return Current connection to the server
     */
    public ConnectionInstance getConnection() {
        return connection;
    }

    /**
     * Current connection status
     */
    private ConnectionStatus connectionStatus = ConnectionStatus.NO_CONNECTED_ONCE;

    /**
     * Gets current ConnectionStatus
     *
     * @return Current connection status
     */
    public ConnectionStatus getConnectionStatus() {
        return connectionStatus;
    }

    /**
     * Derivied constructor
     *
     * @param play PlayerDetails to construct on
     */
    ConnectedPlayer(org.dzingishan.killtheoverlord.game.PlayerDetails play) {
        super(play);
    }

    /**
     * Called when players is connecting to server
     *
     * @param channel
     */
    void connect(Channel channel) {
        connection = new ConnectionInstance(channel, channel.remoteAddress());
        connectionStatus = ConnectionStatus.LOGIN;
    }

    /**
     * Should be called when player successfully login, changes connection
     * status to ONLINE
     */
    void login() {
        connectionStatus = ConnectionStatus.ONLINE;
        loaded = true;
    }

    @Override
    public void AddPlotCard(PlotCardBase b, GameProcess process) {
        P12OwnPlotCardAddEvent p = new P12OwnPlotCardAddEvent();
        p.card = b.getUniqueId();
        sendPacket(p);
        super.AddPlotCard(b, process); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Called when player logout from game
     *
     * @param gameEnded If game still in progress (false) player will be marked
     * as disconnected, otherwise player will be marked as LEFT
     */
    void disconnect(boolean gameEnded) {
        loaded = false;
        connection = null;
        if (gameEnded) {
            connectionStatus = ConnectionStatus.LEFT;
        } else {
            connectionStatus = ConnectionStatus.DISCONNECTED;
        }
    }

    /**
     * Sends packet to the server
     *
     * @param packet Packet to send
     */
    public void sendPacket(Packet packet) {
        System.out.println("Sending packet to player " + connectionStatus);
        if (packet instanceof ServerBoundPacket) {
            throw new IllegalArgumentException("Can't send ServerBound packets!");
        }
        if (connectionStatus == ConnectionStatus.ONLINE) {
            connection.send(packet);
        }
    }

    /**
     * Checks if players is logged and can play game
     *
     * @return True if status == ONLINE, otherwise false
     */
    public boolean hasLoggedIn() {
        return connectionStatus == ConnectionStatus.ONLINE;
    }

    @Override
    public void Debug() {
        System.out.println("Connection status: " + connectionStatus);
        System.out.println("Is connected: " + (connection != null) + "\n------");
        super.Debug();
    }

}
