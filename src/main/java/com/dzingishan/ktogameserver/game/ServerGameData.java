/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.game;

import java.util.ArrayList;
import org.dzingishan.killtheoverlord.game.GameData;
import org.dzingishan.killtheoverlord.game.PlayerDetails;

/**
 * Supertype of GameData, used because of other Player creatation definition
 *
 * @author dzing
 */
public class ServerGameData extends GameData {

    public ServerGameData(PlayerDetails[] details) {
        Players = new ArrayList<>();
        for (PlayerDetails detail : details) {
            Players.add(new ConnectedPlayer(detail));
        }
    }

}
