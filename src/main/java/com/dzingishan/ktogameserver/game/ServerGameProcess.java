/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.game;

import com.dzingishan.ktogameserver.network.GameServer;
import com.dzingishan.ktogameserver.network.login.LoginData;
import com.dzingishan.ktogameserver.network.packets.Packet;
import com.dzingishan.ktogameserver.network.packets.auth.outgoing.P09GameStart;
import com.dzingishan.ktogameserver.network.packets.clientbound.P02PlayersData;
import com.dzingishan.ktogameserver.network.packets.clientbound.P03GameData;
import com.dzingishan.ktogameserver.network.packets.clientbound.P04OwnData;
import io.netty.channel.Channel;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import org.dzingishan.killtheoverlord.events.GameStartEvent;
import org.dzingishan.killtheoverlord.events.PreGameStartEvent;
import org.dzingishan.killtheoverlord.game.CardDatabase;
import org.dzingishan.killtheoverlord.game.DataKeys;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;
import org.dzingishan.killtheoverlord.game.PlayerDetails;

/**
 * Supertype of GameProcess, used during Online-Game
 *
 * @author dzing
 */
public class ServerGameProcess extends GameProcess {

    public GameServer gameServer;
    private Timer timer = new Timer();

    public ServerGameProcess(GameServer gameServer, PlayerDetails[] players) {
        super(new ServerGameData(players));
        this.gameServer = gameServer;
        this.eventManager.registerEventListener(new NetworkEventListener(this));
    }

    /**
     * Sends packet to all players
     *
     * @param packet
     */
    public void sendPacketToAllPlayers(Packet packet) {
        Game.Players.forEach((player) -> {
            ((ConnectedPlayer) player).sendPacket(packet);
        });
    }

    public void stop() {
        timer.cancel();
    }

    @Override
    public void startGame() {
        eventManager.callEvent(new PreGameStartEvent(this));
        P09GameStart start = new P09GameStart();
        start.gameNumber = gameServer.gameNumber;
        start.serverNumber = gameServer.manager.serverNumber;
        gameServer.manager.f.channel().writeAndFlush(start);
        setData(DataKeys.DEATH_ORDER, new ArrayList());
        giveOutCharactersRandom(null);
        //TODO: Debug option mayby?
        modifyCashOfAllPlayers(5);
        addPlotCardsToAllPlayers(4, true);
        getGame().PlayerWithExecution = getGame().GetOverlord();
        waitingForPlayers = true;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                tick();
            }
        }, 0, 300);
        System.out.println("STARTED GAMEq13312231223123123");
    }

    public boolean waitingForPlayers = false;

    /**
     * Called when player logins into the game
     *
     * @param data LoginData
     * @param channel Channel to connect through
     * @return True if found player to log in, otherwise false
     */
    public final boolean playerLoggedIn(LoginData data, Channel channel) {
        for (Player pl : Game.Players) {
            if (pl.PlayerDetails.UniqueNumber == data.uuid) {
                ConnectedPlayer c = (ConnectedPlayer) pl;
                c.connect(channel);
                return true;
            }
        }
        return false;
    }

    private long gameStarted = System.currentTimeMillis();

    @Override
    /**
     * This is the game main thread. You have to execute this function at least
     * every 300 ms (~3 tick/sec)
     */
    public void tick() {
        System.out.println("TICK");
        if (waitingForPlayers) {
            System.out.println("Waiting for players..." + Game.Players.stream().allMatch((o) -> ((ConnectedPlayer) o).getConnectionStatus() == ConnectedPlayer.ConnectionStatus.NO_CONNECTED_ONCE));
            if ((System.currentTimeMillis() - gameStarted) > 10000 && Game.Players.stream().allMatch((o) -> ((ConnectedPlayer) o).getConnectionStatus() == ConnectedPlayer.ConnectionStatus.NO_CONNECTED_ONCE)) {
                gameServer.stop();
            }
            boolean f = false;
            for (Player player : Game.Players) {
                ConnectedPlayer p = (ConnectedPlayer) player;
                System.out.println("Player " + player.PlayerDetails.Nickname + " is " + p.getConnectionStatus() + " " + p.loaded());
                if (p.getConnectionStatus() != ConnectedPlayer.ConnectionStatus.ONLINE || !p.loaded()) {
                    f = true;
                    break;
                }
            }
            if (!f) {
                System.out.println("STARTING GAME");
                waitingForPlayers = false;
                eventManager.callEvent(new GameStartEvent(this));
            }
        } else {
            super.tick();
        }
//        if (allPlayersOnline() && !gameStarted) {
//            //startGame(); //TODO
//        }
    }

    /**
     * Checks if all players are online
     *
     * @return True if all players are online, otherwise false
     */
    public boolean allPlayersOnline() {
        return Game.Players.stream().noneMatch((pl) -> (!((ConnectedPlayer) pl).hasLoggedIn()));
    }

    /**
     * Called player player logouts, TODO: ADD GameEnd
     *
     * @param channel Channel which disconnected
     */
    public void playerDisconnected(Channel channel) {
        if (containsChannel(channel)) {
            Game.Players.stream().filter(o -> ((ConnectedPlayer) o).getConnection().getChannel().equals(channel)).forEach(o -> ((ConnectedPlayer) o).disconnect(false));
        }
        //TODO:
        System.out.println("Stopping gamer");
        gameServer.stop();
    }

    /**
     * Gets current connected Player
     *
     * @param channel Channel connected through
     * @return ConnectedPlayer if found, otherwise null
     */
    public ConnectedPlayer getPlayer(Channel channel) {
        return (ConnectedPlayer) Game.Players.stream().filter(o -> {
            ConnectedPlayer pl = ((ConnectedPlayer) o);
            return (pl.getConnection() != null && pl.getConnection().getChannel().equals(channel));
        }).findFirst().orElse(null);
    }

    /**
     * Checks if any player is logged in using channel
     *
     * @param channel channel to check
     * @return True if someone is logged in
     */
    public boolean containsChannel(Channel channel) {
        return Game.Players.stream().anyMatch((o) -> {
            ConnectedPlayer p = (ConnectedPlayer) o;
            return p.getConnection() != null && p.getConnection().getChannel().equals(channel);
        });
    }

    /**
     * Checks if player logged through this channel successfully logged in
     *
     * @param channel Channel of the player
     * @return
     */
    public boolean hasLoggedIn(Channel channel) {
        return containsChannel(channel) && getPlayer(channel).hasLoggedIn();
    }

    /**
     * Send players data to all players
     */
    public void sendPlayersDataToAll() {

    }

    /**
     * Send player P02PlayersData with current deck situation
     *
     * @param player Player
     */
    public void sendPlayersData(Player player) {
        P02PlayersData data = P02PlayersData.getPacket(this);
        sendPacketToAllPlayers(data);
    }

    /**
     * Changes mode of this channel to Online
     *
     * @param channel
     */
    public void changeMode(Channel channel) {
        getPlayer(channel).login();
    }

    /**
     * Sends game data to player
     *
     * @param player Player to send data to
     */
    public void sendGameData(ConnectedPlayer player) {
        player.sendPacket(new P03GameData(CardDatabase.CHARACTER_VERSION, CardDatabase.PLOT_CARD_VERSION, super.gameStarted));
    }

    /**
     * Sends private player data to player
     *
     * @param player
     */
    public void sendOwnData(ConnectedPlayer player) {
        player.sendPacket(P04OwnData.getOwnData(player));
    }
}
