/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.servermanager;

import com.dzingishan.ktogameserver.network.packets.Packet;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import com.dzingishan.ktogameserver.network.packets.ServerBoundPacket;
import com.dzingishan.ktogameserver.network.packets.auth.ingoing.P28ServerNumber;
import com.dzingishan.ktogameserver.network.packets.auth.ingoing.P29StartTheGame;
import com.dzingishan.ktogameserver.network.packets.auth.ingoing.P30Confirm;
import io.netty.channel.Channel;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Packet Manger, used for getting correct packets and bridges between Game and
 * Server
 *
 * @author dzing
 */
public class PacketManager {

    private NettyServerManager manager;

    private final static Map<Integer, Class<? extends ServerBoundPacket>> SERVER_BOUND = new HashMap<>();
    //private final static Map<Integer, Class<? extends Packet>> LOGIN_BOUND = new HashMap<>();

    static {
        SERVER_BOUND.put(28, P28ServerNumber.class);
        SERVER_BOUND.put(29, P29StartTheGame.class);
        SERVER_BOUND.put(30, P30Confirm.class);
    }

    public PacketManager(NettyServerManager manager) {
        this.manager = manager;
    }

    /**
     * Gets empty ServeBound packet
     *
     * @param frame Frame to get packet based on
     * @return
     */
    public static Packet getServerBoundPacket(PacketFrame frame) {
        Class<? extends Packet> c = SERVER_BOUND.get(frame.number);
        if (c != null) {
            try {
                Packet p = c.newInstance();
                p.frame = frame;
                return p;
            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(PacketManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * Processing incomming Packet, bridges it between Login and Server manager
     *
     * @param packet Packet to process
     * @param channel Channel to get
     */
    public void processIncomingPacket(Packet packet, Channel channel) {
        if (packet instanceof P28ServerNumber) {
            manager.serverNumber = ((P28ServerNumber) packet).serverNumber;
        } else if (packet instanceof P29StartTheGame) {
            P29StartTheGame p = (P29StartTheGame) packet;
            manager.startGame(p.port, p.gameNumber, p.players);
            manager.answer = true;
        } else if (packet instanceof P30Confirm) {
            manager.answer = true;
        }
    }

    void connect() {
        manager.registerPorts();
        manager.startThread();
    }

    void disconnect() {
        manager.timer.cancel();
        manager.timer = new Timer();
    }

}
