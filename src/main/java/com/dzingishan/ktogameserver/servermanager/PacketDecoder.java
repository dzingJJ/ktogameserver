/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.servermanager;

import com.dzingishan.ktogameserver.network.packets.Packet;
import com.dzingishan.ktogameserver.network.packets.PacketFrame;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.util.List;

/**
 * Class which decodes bytes to packet
 *
 * @author dzing
 */
public class PacketDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        //System.out.println("Reading packet");
        if (in.readableBytes() < 4) {
            return;
        }
        int index = in.readerIndex();
        int l = in.readInt();
        if (in.readableBytes() < l) {
            in.readerIndex(index);
            return;
        }
        byte[] bytes = new byte[l];
        in.readBytes(bytes);
        //System.out.println(Arrays.toString(bytes));
        ByteBuf b = Unpooled.copiedBuffer(bytes);
        int nr = b.readInt();
        Packet p = PacketManager.getServerBoundPacket(new PacketFrame(nr));
        p.read(b);
        out.add(p);
    }

}
