/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dzingishan.ktogameserver.servermanager;

import com.dzingishan.ktogameserver.network.GameServer;
import com.dzingishan.ktogameserver.network.packets.auth.outgoing.P06ServerRegister;
import com.dzingishan.ktogameserver.network.packets.auth.outgoing.P08ServerOnlineTick;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.util.Timer;
import java.util.TimerTask;
import org.dzingishan.killtheoverlord.game.PlayerDetails;

/**
 *
 * @author dzing
 */
public class NettyServerManager {

    public int serverNumber;
    private EventLoopGroup workerGroup;
    public ChannelFuture f;

    private String address = "localhost";

    private int[] ports = new int[]{1234, 1235};

    private PacketManager manager = new PacketManager(this);

    private Timer connThread = new Timer();

    public void startServer(final String address, final int port) {
        connThread.schedule(new TimerTask() {
            @Override
            public void run() {
                connectionFailedThread(address, port);
            }
        }, 0, 2000);
    }
    public Timer timer = new Timer();

    public void connectionFailedThread(String address, int port) {
        if (f == null || (f.isDone() && !f.channel().isActive())) {
            System.out.println("Trying to connect");
            createClient(address, port);
        }
    }

    public void startThread() {

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                serverTick();
            }
        }, 0, 1500);

    }

    public boolean answer = true;

    private void serverTick() {
        if (answer) {
            P08ServerOnlineTick t = new P08ServerOnlineTick();
            t.serverNumber = serverNumber;
            f.channel().writeAndFlush(t);
            answer = false;
        }
    }

    public void registerPorts() {
        P06ServerRegister register = new P06ServerRegister();
        register.address = address;
        register.ports = ports;
        f.channel().writeAndFlush(register);
        System.out.println("Registered!");
    }

    private void createClient(String address, int port) {
        workerGroup = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        b.group(workerGroup)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                System.out.println("Initialized Channel!");
                        ch.pipeline().addLast(new PacketEncoder()).addLast(new PacketDecoder()).addLast(new InboundController(manager));
                    }
                });
        f = b.connect(address, port);
        System.out.println("Connection!");
    }

    void startGame(int port, int gameNumber, PlayerDetails[] players) {
        new GameServer(1, this).start(players, port, gameNumber);//TODO Create Thread
    }

}
